# ics-ans-role-ioc-base

Ansible role to install the base requirements for an IOC.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
# ioc user and group
ioc_base_user_name: iocuser
ioc_base_user_id: 1042
ioc_base_group_name: iocgroup
ioc_base_group_id: 1042
# Channel Access port for iptables rule to convert UDP unicat to broadcast
ioc_base_ca_port: 5064
# IOC git folder version to clone
ioc_base_ioc_version: master
ioc_base_gitlab_namespace: ioc
ioc_base_gitlab_user: user
ioc_base_gitlab_token: xxxxxx
ioc_base_gitlab_url: "https://{{ ioc_base_gitlab_user }}:{{ ioc_base_gitlab_token }}@gitlab.esss.lu.se"
# hostname of the nonvolatile server
# If not empty, the directory /export/nonvolatile/{{ ansible_fqdn }} will be created
# on that machine and will be mounted locally
# We use "" string by default so that it's easy to disable at host level
ioc_base_nonvolatile_server: ""
# Local nonvolatile directory
ioc_base_nonvolatile_directory: /opt/nonvolatile
# Extra required mount points (shouldn't include the nonvolatile server)
# Should be something like:
# ioc_base_nfs_mountpoints:
#   - src: "eee-01:/export/epics"
#     mountpoint: /opt/epics
#     opts: ro
#   - src: "eee-01:/export/startup"
#     mountpoint: /opt/startup
#     opts: ro
ioc_base_nfs_mountpoints: []
# Boolean to install the ess-boot service or not
# Only for backward compatibility. Should be disabled if possible.
ioc_base_boot_service: true
# Boolean used to avoid restarting services during kickstart installlation
ioc_base_is_anaconda_installation: false
# procServ requires the EPEL, hence the dependency to the repository role
ioc_base_packages:
  - autoconf
  - automake
  - bash-completion
  - boost-devel
  - coreutils
  - cpan
  - curl
  - darcs
  - dkms
  - gcc-c++
  - git
  - graphviz
  - hdf5-devel
  - hg
  - ipmitool
  - kernel-devel
  - lesstif-devel
  - libXmu-devel
  - libXp-devel
  - libXpm-devel
  - libXt-devel
  - libcurl-devel
  - liberation-fonts-common
  - liberation-mono-fonts
  - liberation-narrow-fonts
  - liberation-sans-fonts
  - liberation-serif-fonts
  - libjpeg-turbo-devel
  - libpng12-devel
  - libraw1394.x86_64
  - libtirpc-devel
  - libtool
  - libusb-devel
  - libusbx-devel
  - libxml2-devel
  - libzip-devel
  - logrotate
  - m4
  - ncurses-devel
  - net-snmp
  - net-snmp-devel
  - net-snmp-utils
  - nfs-utils
  - opencv-devel
  - patch
  - pcre-devel
  - perl-devel
  - procServ
  - python-devel
  - re2c
  - readline-devel
  - symlinks
  - systemd-devel
  - tclx
  - telnet
  - tree
  - xorg-x11-fonts-misc
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc-base
```

## License

BSD 2-clause
